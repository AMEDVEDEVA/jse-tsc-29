package ru.tsc.golovina.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractOwnerEntity extends AbstractEntity {

    @Nullable
    protected String userId;

    @NotNull
    protected String name = "";

    @NotNull
    protected String description = "";

    @NotNull
    protected Status status = Status.NOT_STARTED;

    @Nullable
    protected Date startDate;

    @Nullable
    protected Date finishDate;

    @NotNull
    protected Date created = new Date();

    @Override
    public String toString() {
        return id + ": " + name;
    }

}
