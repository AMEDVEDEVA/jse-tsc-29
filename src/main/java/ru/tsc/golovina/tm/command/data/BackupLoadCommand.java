package ru.tsc.golovina.tm.command.data;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.dto.Domain;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BackupLoadCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "backup-load";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public String getDescription() {
        return "Load backup from XML.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final File file = new File(BACKUP_XML);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(BACKUP_XML)));
        @NotNull final XmlMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}
