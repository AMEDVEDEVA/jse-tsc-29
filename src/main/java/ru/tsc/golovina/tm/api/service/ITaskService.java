package ru.tsc.golovina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Task;

public interface ITaskService extends IOwnerService<Task> {

    void create(@Nullable String userId, @Nullable String name);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task removeByName(@Nullable String userId, @Nullable String name);

    void updateById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    boolean existsByName(@Nullable String userId, @NotNull String name);

    @NotNull
    Task findByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task startById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task startByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task startByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task finishById(@Nullable String userId, @Nullable String id);

    @NotNull
    Task finishByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull
    Task finishByName(@Nullable String userId, @Nullable String name);

    @NotNull
    Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status);

}
