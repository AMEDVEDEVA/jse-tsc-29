package ru.tsc.golovina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @NotNull
    AbstractCommand getCommandByName(@NotNull String name);

    @NotNull
    AbstractCommand getCommandByArg(@Nullable String arg);

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getArgNames();

    void add(@NotNull AbstractCommand command);
}
