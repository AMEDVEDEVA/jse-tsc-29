package ru.tsc.golovina.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.golovina.tm.api.repository.ITaskRepository;
import ru.tsc.golovina.tm.enumerated.Status;
import ru.tsc.golovina.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return findAll(userId).stream()
                .filter(t -> t.getProjectId().equals(projectId))
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsByName(@NotNull final String userId, @NotNull final String name) {
        return findByName(userId, name) != null;
    }

    @Override
    public void bindTaskToProjectById(@NotNull final String userId, @NotNull final String projectId,
                                      @NotNull final String taskId) {
        @NotNull final Task task = findById(taskId);
        task.setProjectId(projectId);
        task.setUserId(userId);
    }

    @Override
    public void unbindTaskById(@NotNull final String userId, @NotNull final String taskId) {
        @NotNull final Task task = findById(taskId);
        task.setUserId(null);
        task.setProjectId(null);
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        findAll(userId).stream()
                .filter(t -> projectId.equals(t.getProjectId()))
                .forEach(t -> t.setProjectId(null));
    }

    @Nullable
    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(t -> t.getName().equals(name))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Nullable
    @Override
    public Task startById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @NotNull
    @Override
    public Task startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Nullable
    @Override
    public Task startByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Nullable
    @Override
    public Task finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @NotNull
    @Override
    public Task finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Nullable
    @Override
    public Task finishByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public Task changeStatusByIndex(@NotNull final String userId, @NotNull final Integer index,
                                    @NotNull final Status status) {
        @NotNull final Task task = findByIndex(userId, index);
        task.setStatus(status);
        return task;
    }

    @Nullable
    @Override
    public Task changeStatusByName(@NotNull final String userId, @NotNull final String name,
                                   @NotNull final Status status) {
        @Nullable final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

}
