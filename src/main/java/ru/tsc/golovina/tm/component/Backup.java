package ru.tsc.golovina.tm.component;

import lombok.SneakyThrows;

public class Backup extends Thread {

    private static final String COMMAND_SAVE = "backup-save";

    private static final String COMMAND_LOAD = "backup-load";

    private static final int INTERVAL = 30000;

    private final Bootstrap bootstrap;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (true) {
            save();
            Thread.sleep(INTERVAL);
        }
    }

    public void init() {
        load();
        start();
    }

    @SneakyThrows
    public void save() {
        bootstrap.runCommand(COMMAND_SAVE);
    }

    @SneakyThrows
    public void load() {
        bootstrap.runCommand(COMMAND_LOAD);
    }

}
